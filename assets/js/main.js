var swiperhomeNews = '';
var swiperhomeProduct = '';

function swipers() {
    if ($('.swiper-container-home_news').length) {
        swiperhomeNews = new Swiper('.swiper-container-home_news', {
            slidesPerView: 'auto',
            centeredSlides: true,
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev',
            },
        });
    };
    if ($('.swiper-container-home_product').length) {
        swiperhomeNews = new Swiper('.swiper-container-home_product', {
            slidesPerView: 'auto',
            centeredSlides: false,
            spaceBetween: 30,
            grabCursor: true,
            freeMode: true,
        });
    };
};
// scroll effect on anchor tag to id
function scroll() {
    $('a[href^="#"]').click(function() {
        var href = $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $('html, body').stop().animate({
            scrollTop: position
        }, 500, 'swing');
        return false;
    });
};

$('header .header__content-fontsize #big').click(function(event) {
    $('header .header__content-fontsize #big').addClass('active-font');
    $('body').addClass('big');
    $('body').removeClass('standard');
    $('header .header__content-fontsize #standard').removeClass('active-font');
});
$('header .header__content-fontsize #standard').click(function(event) {
    $('header .header__content-fontsize #standard').addClass('active-font');
    $('body').removeClass('big');
    $('body').addClass('standard');
    $('header .header__content-fontsize #big').removeClass('active-font');
});

$('.more-menu').click(function() {
  $('.sp-menu__content-sub').slideToggle("100");
  $(this).toggleClass('open');
});

$('.burger-menu').click(function() {
  $('.burger-menu').toggleClass('burger-menu__active');
  $('header').toggleClass('header-active');
  $('.sp-menu__content').toggleClass('sp-menu__content-active');

  const $menu = $('.burger-menu, .header__content, .sp-menu__content');

  $(document).mouseup(function (e) {
    if (!$menu.is(e.target) // if the target of the click isn't the container...
        && $menu.has(e.target).length === 0) // ... nor a descendant of the container
    {
      if ($('.burger-menu ').hasClass('burger-menu__active')) {
          $('.burger-menu').removeClass('burger-menu__active');
          $('header').removeClass('header-active');
          $('.sp-menu__content').removeClass('sp-menu__content-active');
      }
    }
  });
});

// Check if in viewport from bottom
$.fn.isInViewport = function() {
    var elementTop         = $(this).offset().top;
          elementBottom      = elementTop + $(this).outerHeight();
          viewportTop      = $(window).scrollTop();
          viewportBottom     = viewportTop + $(window).height();
          thisHalfHeight     = $(this).outerHeight();
    // return (elementBottom + 0) > viewportTop && (elementBottom - 0) < viewportBottom;
    return elementBottom > viewportTop && elementTop < viewportBottom;
};
// check visible height in viewport
;(function($, win) {
  $.fn.visibleInViewport = function(cb) {
     return this.each(function(i,el) {
       function visPx(){
         var elH = $(el).outerHeight(),
             H = $(win).height(),
             r = el.getBoundingClientRect(), t=r.top, b=r.bottom;
         return cb.call(el, Math.max(0, t>0? Math.min(elH, H-t) : Math.min(b, H)));  
       }
       visPx();
       $(win).on("resize scroll", visPx);
     });
  };
}(jQuery, window));

//hide "back-to-top"
function hideToTop() {
  $('header').each(function() {
    if ($(this).isInViewport()) {
      $('#footer .to-top').addClass('hide-top');
    } else {
      $('#footer .to-top').removeClass('hide-top');
    }
  });

  $('footer').each(function() {
    if ($(this).isInViewport()) {
      $('#footer .to-top').addClass('color-negative');
    } else {
      $('#footer .to-top').removeClass('color-negative');
    }
  })
} 


$(function() {
    scroll();
    swipers();
});

$(window).on('load', function() {
}); // load

$(window).on('resize', function() {

}); // resize

$(window).scroll(function() {
    hideToTop();
}); // scroll

$(window).on('load resize', function() {

}); // load resize
$(window).on('ready', function() {
    swipers();


}); // ready 